WORKDIR /usr/share/nginx/html
RUN npm install
FROM nginx:stable-alpine as production-stage
RUN apt-get update && apt-get install -y \
    vim \
    curl \
    git 
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
